#pragma once

#include <nlohmann/json.hpp>

#include "CaculatePlateValues.h"

namespace gatewatcher
{
    class GateWatcher
    {
    public:
        GateWatcher();

        void Run();
        void LoadConfigFile();

    private:
        const ConfigAlpr m_brConfigAlpr;
        const ConfigAlpr m_br2ConfigAlpr;
        nlohmann::json m_configFile;

        static std::string currentDatetime();
        std::string run();
    };
}
