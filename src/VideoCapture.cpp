#include "VideoCapture.h"

#include <cpr/cpr.h>

#include <iostream>

namespace gatewatcher
{
    VideoCapture::VideoCapture(const std::string& str) : m_url(str) {}

    std::string VideoCapture::Perform()
    {
        auto r = cpr::Get(cpr::Url{m_url});
        return r.text;
    }
}