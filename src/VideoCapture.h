#pragma once

#include <string>

namespace gatewatcher
{
    class VideoCapture
    {
    public:
        VideoCapture(const std::string& string);
        std::string Perform();

    private:
        const std::string m_url;
    };
}

// http://<user>:<password>@<ip>:<port>/cgi-bin/snapshot.cgi