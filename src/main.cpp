#include <fstream>
#include <iostream>

#include "CaculatePlateValues.h"
#include "GateWatcher.h"
#include "VideoCapture.h"

void test_video_capture() {}

int main()
{
    try
    {
        gatewatcher::GateWatcher obj;
        obj.Run();
    }
    catch (const std::exception& e)
    {
        std::cout << e.what();
    }

    return 0;
}
